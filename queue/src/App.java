import java.util.Scanner;

public class App {
        public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        Queue queue = new Queue(5);

        for(int i=0;i<queue.maxSize;i++){
            queue.insert(input.nextInt());
        }

        System.out.println("Queue elements:");
        queue.displayQueue();
        

        for(int i=0;i<queue.maxSize;i++){
            queue.remove();
        }

        System.out.println("Queue elements when delete element:");
        queue.displayQueue();
    }
}
